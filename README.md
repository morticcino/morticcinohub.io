# morticcino.github.io

A new way to quickly create your vector creation!

Thanks to:

shape recognizer
https://depts.washington.edu/aimgroup/proj/dollar/ndollar.html

Stroke simplifier
http://mourner.github.io/simplify-js/

JS color picker
http://jscolor.com/

Gest recognition
http://hammerjs.github.io/

Custom font
http://flat-it.com
